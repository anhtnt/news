## Thư mục
- Gom phần logic và views của admin vào từng thư mục tương ứng trong app, views và public.

## Container

- Dùng PHP-DI vì có autowiring và hỗ trợ share object tự động.
- Đã tìm hiểu Pimple và League/Container, nhưng Pimple không có autowiring, còn League không hỗ trợ shared objects khi set autowwiring.

## App

- Là wrapper của container, dùng decorator pattern.
- Get các object nhanh hơn do tự động thêm prefix "App\". Fallback về normal class.

## Request

- Tự viết 1 class nhận và lấy tham số từ request. Chỉ hỗ trợ $_GET và $_POST.
- Uri tự viết mô phỏng PSR, hoạt động với base path của app (tức là app có thể để trong thư mục con).

## Router

- Dùng regex để match rule và gọi 1 callable khi khớp.
- Hàm callable nhận tham số đầu tiên là Request, tương tự như Slimframework.
- Có hàm helper tự động generate các routes cho 1 resource CRUD, tương tự như Laravel.

## Database

- Sử dụng SQLite, đơn giản và phù hợp với các app nhỏ.
- Sử dụng PDO sẵn có, vốn đã là 1 DB abstraction layer rồi.
- Phải cấu hình cho web app có quyền ghi lên file database và cả thư mục chứa file đó.

## Form

- CRFS
    - Dùng thư viện [Anti-CSRF](https://github.com/paragonie/anti-csrf).
    - Tạo 1 middleware Csrf trong App\Http để kiểm tra form khi POST và throw 1 Exception nếu invalid. Chạy tự động kiểm tra tất cả các request dạng POST.
    - Tạo 1 hàm helper csrf() để output trường input ẩn chứa token. Hiện view này vẫn gọi trực tiếp đến class. TODO: tìm cách không gọi trực tiếp đến class.

## Authentication

- Dùng session để lưu ID của user hiện tại.
- User chỉ dùng username/password, không có email nên không có tính năng forget password.

## Resources (CRUD)

- Mỗi 1 resource sẽ nằm trong 1 thư mục, có các class Collection và Model. Riêng controller tách riêng cho phần backend và frontend.
- Lấy từ DB dùng PDO::FETCH_CLASS để tự động tạo object theo đúng model.

## Icons

- Dùng icon font [Feather](https://feathericons.com/).

## Debug

- Bật debug ở file `config.php`. File này sẽ load file `local-config.php` dùng để setup môi trường local khi develop. File này được ghi trong `.gitignore` để không upload lên host.
