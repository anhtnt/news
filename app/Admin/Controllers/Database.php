<?php
namespace App\Admin\Controllers;

class Database
{
    public $db;

    public function __construct()
    {
        $this->db = app('db');
    }

    public function create()
    {
        $this->db->query('
            CREATE TABLE IF NOT EXISTS items (
                id INTEGER PRIMARY KEY,
                title TEXT NOT NULL,
                url TEXT NOT NULL,
                date TEXT NOT NULL,
                views INTEGER NOT NULL DEFAULT 0,
                status TEXT NOT NULL
            )
        ');
    }
}
