<?php
namespace App\Admin\Controllers;

use Atom\Resources\Base\Controller;
use App\Resources\Item\Collection;
use Atom\Http\Request;
use Atom\Controllers\Fetch;

class Item extends Controller
{
    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function approve(Request $request, $id)
    {
        $this->collection->approve($id);
        redirect($request->getParam('current_url'), false, 'Item has been approved.', 'success');
    }

    public function sanitize($data)
    {
        $data = array_merge([
            'url' => '',
            'title' => '',
            'status' => 'pending',
        ], $data);
        $data['url'] = strtok($data['url'], '?');
        $data['title'] = strip_tags($data['title']);
        return $data;
    }

    public function validateCreate($data)
    {
        if (!$this->validateData($data)) {
            return false;
        }

        if ($this->collection->has($data['url'])) {
            $this->errors = 'The URL already exists.';
            return false;
        }

        return true;
    }

    public function validateUpdate($data)
    {
        return $this->validateData($data);
    }

    protected function validateData($data)
    {
        if (!$data['title'] || !$data['url'] || !$data['status']) {
            $this->errors = 'Please enter all details when submitting.';
            return false;
        }
        if (!filter_var($data['url'], FILTER_VALIDATE_URL)) {
            $this->errors = 'Invalid URL.';
            return false;
        }

        return true;
    }
}
