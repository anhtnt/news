<?php
namespace App\Admin\Controllers;

use Atom\Http\Request;

class Settings
{
    public $db;
    public $table = 'settings';
    public $keys = [
        'site_title',
        'site_description',
        'feedly_user_id',
        'feedly_access_token',
        'feedly_limit',
        'disable_registration',
    ];

    public function __construct()
    {
        $this->db = app('db');
    }

    public function index(Request $request)
    {
        $rows = $this->db->select($this->table, ['key', 'value']);

        $settings = array_fill_keys($this->keys, '');
        foreach ($rows as $row) {
            $settings[$row['key']] = $row['value'];
        }

        view('admin.settings', [
            'title' => 'Settings',
            'settings' => $settings,
        ]);
    }

    public function store(Request $request)
    {
        foreach ($this->keys as $key) {
            $value = $request->getParam($key);

            if (!$value) {
                $this->db->delete($this->table, ['key' => $key]);
                continue;
            }

            if ($this->has($key)) {
                $ok = $this->db->update($this->table, ['value' => $value], ['key' => $key]);
                continue;
            }

            $this->db->insert($this->table, compact('key', 'value'));
        }
        back('Settings updated', 'success');
    }

    public function has($key)
    {
        return $this->db->has($this->table, ['key' => $key]);
    }
}
