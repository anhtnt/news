<?php
namespace App\Controllers;

class Clean
{
    public function clean()
    {
        $yesterday = date('Y-m-d H:i:s', strtotime('-1 day'));
        app('db')->delete('items', ['status' => 'pending', 'date[<]' => $yesterday]);
        if ('cli' !== php_sapi_name()) {
            redirect('/admin/items', true, 'Old pending items have been removed successfully.', 'success');
        }
    }
}
