<?php
namespace App\Controllers;

use App\Services\Feedly;
use App\Resources\Item\Collection;

class Fetch
{
    public $feedly;
    public $collection;
    public $count;

    public function __construct(Feedly $feedly, Collection $collection)
    {
        $this->feedly = $feedly;
        $this->collection = $collection;
    }

    public function fetch()
    {
        $items = $this->feedly->fetch();
        $this->count = 0;
        array_walk($items, [$this, 'store']);
        if ('cli' !== php_sapi_name()) {
            redirect('/admin/items', true, "$this->count items have been added.", 'success');
        } else {
            return $this->count;
        }
    }

    public function store($item)
    {
        if (!$this->collection->has($item['url'])) {
            $this->collection->create($item);
            $this->count++;
        }
    }
}
