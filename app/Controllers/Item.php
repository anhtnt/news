<?php
namespace App\Controllers;

use App\Formatter;
use App\Resources\Item\Collection;
use Atom\Http\Request;

class Item
{
    public $collection;
    public $errors;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function index(Request $request)
    {
        $page = max(1, intval($request->getParam('page')));
        $args = ['page' => $page, 'limit' => 24, 'status' => 'active'];

        $items = $this->collection->query($args);
        $pagination = $this->collection->pagination($args);
        array_walk($items, [$this, 'parse']);

        view('item.index', [
            'items'      => $items,
            'pagination' => $pagination,
        ]);
    }

    public function create()
    {
        return view('item.create');
    }

    public function store(Request $request)
    {
        $data = $request->getParams();
        $this->sanitize($data);
        $data = array_map('trim', $data);
        if (!$this->validateCreate($data)) {
            back($this->errors, 'error');
        }

        $this->collection->create($data);
        back('Thank you. Your article has been submitted successfully.', 'success');
    }

    public function sanitize(&$data)
    {
        $data = array_merge([
            'url' => '',
            'title' => '',
            'status' => 'pending',
        ], $data);
        $data['url'] = strtok($data['url'], '?');
        $data['title'] = strip_tags($data['title']);
    }

    public function validateCreate($data)
    {
        if (!$data['title'] || !$data['url'] || !$data['status']) {
            $this->errors = 'Please enter all details when submitting.';
            return false;
        }
        if (!filter_var($data['url'], FILTER_VALIDATE_URL)) {
            $this->errors = 'Invalid URL.';
            return false;
        }

        if ($this->collection->has($data['url'])) {
            $this->errors = 'This URL already exists.';
            return false;
        }

        return true;
    }

    public function show(Request $request, $id)
    {
        $item = $this->collection->get('id', $id);
        $views = $item['views'] + 1;
        $this->collection->update($id, ['views' => $views]);
        return redirect($item['url'], false);
    }

    public function parse(&$item)
    {
        $item['domain'] = Formatter::domain($item['url']);
        $item['formattedTime'] = Formatter::humanTimeDiff($item['date']);
    }
}
