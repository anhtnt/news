<?php
namespace App\Controllers;

use App\Resources\Item\Collection;

class Rss
{
    public $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function index()
    {
        $args = ['limit' => 10, 'status' => 'active'];
        $items = $this->collection->query($args);

        view('rss', ['items' => $items]);
    }
}
