<?php
namespace App;

class Formatter
{
    public static function domain($url)
    {
        return str_replace('www.', '', parse_url($url, PHP_URL_HOST));
    }

    public static function humanTimeDiff($from)
    {
        $from  = strtotime($from);
        $to    = time();
        $diff  = (int)abs($to - $from);
        $since = '';
        if ($diff < 3600) {
            $mins = round($diff / 60);
            if ($mins <= 1) {
                $mins = 1;
            }
            $since = 1 == $mins ? sprintf('%s min', $mins) : sprintf('%s mins', $mins);
        } elseif ($diff < 86400 && $diff >= 3600) {
            $hours = round($diff / 3600);
            if ($hours <= 1) {
                $hours = 1;
            }
            $since = 1 == $hours ? sprintf('%s hour', $hours) : sprintf('%s hours', $hours);
        } elseif ($diff < 604800 && $diff >= 86400) {
            $days = round($diff / 86400);
            if ($days <= 1) {
                $days = 1;
            }
            $since = 1 == $days ? sprintf('%s day', $days) : sprintf('%s days', $days);
        } elseif ($diff < 2592000 && $diff >= 604800) {
            $weeks = round($diff / 604800);
            if ($weeks <= 1) {
                $weeks = 1;
            }
            $since = 1 == $weeks ? sprintf('%s week', $weeks) : sprintf('%s weeks', $weeks);
        } elseif ($diff < 31526000 && $diff >= 2592000) {
            $months = round($diff / 2592000);
            if ($months <= 1) {
                $months = 1;
            }
            $since = 1 == $months ? sprintf('%s month', $months) : sprintf('%s months', $months);
        } elseif ($diff >= 31526000) {
            $years = round($diff / 31526000);
            if ($years <= 1) {
                $years = 1;
            }
            $since = 1 == $years ? sprintf('%s year', $years) : sprintf('%s years', $years);
        }

        return $since . ' ago';
    }
}
