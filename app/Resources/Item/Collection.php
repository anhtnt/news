<?php
namespace App\Resources\Item;

use Atom\Resources\Base\Collection as BaseCollection;

class Collection extends BaseCollection
{
    public $table = 'items';
    public $fields = ['id', 'title', 'url', 'date', 'views', 'status'];

    public $defaultQueryArgs = [
        'order_by' => 'date',
        'order'    => 'DESC',
        'limit'    => 10,
        'page'     => 1,
    ];

    public function has($url)
    {
        return $this->db->has($this->table, ['url' => $url]);
    }

    public function create($data)
    {
        $data = array_intersect_key($data, array_flip($this->fields));
        $data['date'] = date('Y-m-d H:i:s');
        $data['views'] = 0;
        $data['status'] = $data['status'] ?? 'pending';

        $this->db->insert($this->table, $data);
    }

    public function update($id, $data)
    {
        $data = array_intersect_key($data, array_flip($this->fields));
        $this->db->update($this->table, $data, ['id' => $id]);
    }

    public function approve($id)
    {
        $this->db->update($this->table, ['status' => 'active'], ['id' => $id]);
    }

    public function where($args)
    {
        $where = [];
        $status = $args['status'] ?? '';
        if (!$status) {
            $status = $this->request->getParam('status');
        }
        if ($status) {
            $where['status'] = $status;
        }
        if ($s = $this->request->getParam('s')) {
            $where['title[~]'] = $s;
        }
        return $where;
    }

    public function count($status)
    {
        return $this->db->count($this->table, ['status' => $status]);
    }
}
