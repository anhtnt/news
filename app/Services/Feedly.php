<?php
namespace App\Services;

class Feedly
{
    public $baseUrl = 'https://cloud.feedly.com';

    public function fetch()
    {
        $url = sprintf(
            '%s/v3/streams/contents?streamId=user/%s/category/global.all&count=%d',
            $this->baseUrl,
            setting('feedly_user_id'),
            setting('feedly_limit')
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $headers = ['Authorization: OAuth ' . setting('feedly_access_token')];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $data = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($data);
        if (isset($data->errorMessage)) {
            echo "Error: $data->errorMessage\n";
        }
        if (empty($data->items)) {
            return [];
        }
        return array_map([$this, 'parseItem'], $data->items);
    }

    public function parseItem($item)
    {
        return [
            'title'  => trim($item->title),
            'date'   => date('Y-m-d H:i:s'),
            'url'    => $this->getUrl($item),
            'status' => 'pending',
        ];
    }

    public function getUrl($item)
    {
        $url = '';

        if (isset($item->canonicalUrl)) {
            $url = $item->canonicalUrl;
        } elseif (isset($item->canonical)) {
            $url = $item->canonical[0]->href;
        } elseif (isset($item->alternate)) {
            $url = $item->alternate[0]->href;
        }
        if (! $url) {
            return $url;
        }

        if (false !== strpos($url, 'feedproxy.google.com') || false !== strpos($url, 'feeds.feedblitz.com')) {
            $url = $this->getOriginalUrl($url);
        }
        return strtok($url, '?');
    }

    public function getOriginalUrl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $output = curl_exec($ch);
        curl_close($ch);

        $headers = [];
        $data = explode("\n", $output);
        $headers['status'] = $data[0];
        array_shift($data);

        foreach ($data as $part) {
            list($header, $value) = explode(':', $part);
            $headers[trim($header)] = trim($value);
        }

        return $headers['Location'] ?? $url;
    }
}
