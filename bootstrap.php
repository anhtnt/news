<?php
require __DIR__.'/vendor/autoload.php';

$router = app('Router');

/*****************************
 * Admin
 *****************************/

// Dashboard.
$router->get('/admin/', 'Admin\Controllers\Item::index');

// Resources.
$router->resource('/admin/items', 'Admin\Controllers\Item');
$router->post('/admin/items/(\d+)/approve', 'Admin\Controllers\Item::approve');
$router->get('/admin/items/fetch', 'Controllers\Fetch::fetch');
$router->get('/admin/items/clean', 'Controllers\Clean::clean');

// Settings.
$router->get('/admin/settings', 'Admin\Controllers\Settings::index');
$router->post('/admin/settings', 'Admin\Controllers\Settings::store');

/*****************************
 * Frontend
 *****************************/

// Homepage.
$router->get('/', 'Controllers\Item::index');

// Submit page.
$router->get('/submit', 'Controllers\Item::create');
$router->post('/submit', 'Controllers\Item::store');

// View single item.
$router->get('/item/(\d+)', 'Controllers\Item::show');

// RSS.
$router->get('/rss', 'Controllers\Rss::index');
