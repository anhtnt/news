<?php
require __DIR__.'/vendor/autoload.php';

echo "Cleaning old pending items...\n";
app('Controllers\Clean')->clean();
echo 'Done.';
