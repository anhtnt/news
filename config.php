<?php
if (file_exists(__DIR__ . '/local-config.php')) {
    return include 'local-config.php';
}

return [
    'site'   => [
        'url'   => '/',
        'title' => 'News',
    ],
];
