<?php
require __DIR__.'/vendor/autoload.php';

echo "Fetching new items...\n";
$count = app('Controllers\Fetch')->fetch();
echo "Done. $count items have been added.";
