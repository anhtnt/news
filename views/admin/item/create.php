<?php view('admin.partials.header') ?>

<h1 class="page-title"><?= $title ?></h1>

<?php messages() ?>

<form method="POST" action="<?= url('/admin/items/create') ?>" class="form-post">
    <?php csrf() ?>

    <p>
        <input type="text" name="title" required placeholder="Title">
    </p>
    <p>
        <input type="url" name="url" required placeholder="URL">
    </p>
    <p>
        <label class="d-inline-block"><input type="radio" name="status" value="active" checked> Active</label>
        <label class="d-inline-block"><input type="radio" name="status" value="pending"> Pending</label>
    </p>

    <button>Submit</button>
</form>

<?php view('admin.partials.footer') ?>
