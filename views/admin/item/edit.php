<?php view('admin.partials.header') ?>

<h1 class="page-title"><?= $title ?> <span class="text-muted">#<?= $item['id'] ?></span></h1>

<?php messages() ?>

<form method="POST" action="<?= url("/admin/items/{$item['id']}/edit") ?>" class="form-post">
    <?php csrf() ?>

    <p>
        <input type="text" name="title" value="<?= $item['title'] ?>" required placeholder="Title">
    </p>
    <p>
        <input type="url" name="url" value="<?= $item['url'] ?>" required placeholder="URL">
    </p>
    <p>
        <label class="d-inline-block"><input type="radio" name="status" value="active"<?= 'active' === $item['status'] ? ' checked' : '' ?>> Active</label>
        <label class="d-inline-block"><input type="radio" name="status" value="pending"<?= 'pending' === $item['status'] ? ' checked' : '' ?>> Pending</label>
    </p>

    <button>Update</button>
</form>

<?php view('admin.partials.footer') ?>
