<?php view('admin.partials.header') ?>

<h1 class="page-title"><?= $title ?></h1>

<?php messages() ?>

<div class="d-flex justify-content-between">
    <form action="" method="get">
        <?php $status = filter_input(INPUT_GET, 'status') ?>
        <select name="status" class="form-control-sm">
            <option value="">All statuses</option>
            <option value="active"<?= 'active' === $status ? ' selected' : '' ?>>Active</option>
            <option value="pending"<?= 'pending' === $status ? ' selected' : '' ?>>Pending</option>
        </select>
        <button class="btn-sm btn-secondary">Filter</button>
    </form>
    <form action="" method="get" class="search">
        <?php $term = filter_input(INPUT_GET, 's') ?>
        <input type="search" name="s" class="form-control-sm" value="<?= $term ?>" placeholder="Search...">
        <button class="btn-sm btn-transparent"><i data-feather="search"></i></button>
    </form>
</div>

<div class="items" id="items">
    <table>
        <tr>
            <th>Title</th>
            <th>Date</th>
            <th>Views</th>
            <th></th>
        </tr>
        <?php foreach ($items as $item): ?>
            <tr>
                <td>
                    <a href="<?= url("/item/{$item['id']}") ?>" class="d-block" target="_blank">
                        <?= $item['title'] ?>
                        <?php if ('pending' === $item['status']): ?>
                            <span class="status status-warning"></span>
                        <?php endif ?>
                        <small class="text-muted d-block"><?= App\Formatter::domain($item['url']) ?></small>
                    </a>
                </td>
                <td><?= date('d/m/Y', strtotime($item['date'])) ?></td>
                <td><?= $item['views'] ?></td>
                <td>
                    <div class="d-flex justify-content-end">
                        <?php
                        $uri = app('Http\Uri');
                        $itemsUrl = '/admin/items' === $uri->getPath() ? '' : url('/admin/items');
                        ?>
                        <?php if ('pending' === $item['status']): ?>
                            <form method="POST" action="<?= url("/admin/items/{$item['id']}/approve") ?>">
                                <?php csrf(url("/admin/items/{$item['id']}/approve")) ?>
                                <input type="hidden" name="current_url" value="<?= $uri->current() ?>">
                                <button class="btn-sm btn-transparent"><i data-feather="check-circle"></i></button>
                            </form>
                        <?php else: ?>
                            <form method="POST" action="<?= $itemsUrl ?>">
                                <?php csrf($itemsUrl) ?>
                                <input type="hidden" name="id" value="<?= $item['id'] ?>">
                                <button class="btn-sm btn-transparent delete"><i data-feather="trash-2"></i></button>
                            </form>
                        <?php endif; ?>
                    </div>
                </td>
            </tr>
        <?php endforeach ?>
    </table>
</div>

<?php view('admin.partials.pagination') ?>
<?php view('admin.partials.script-delete') ?>
<?php view('admin.partials.footer') ?>
