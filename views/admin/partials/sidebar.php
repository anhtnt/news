<aside class="sidebar col-lg-2 col-sm-3">
    <nav>
        <ul class="menu">
            <li><a href="<?= url('/admin/items') ?>"><i data-feather="file"></i> Items</a></li>
            <li><a href="<?= url('/admin/users') ?>"><i data-feather="users"></i> Users</a></li>
            <li><a href="<?= url('/admin/settings') ?>"><i data-feather="settings"></i> Settings</a></li>
        </ul>
        <p><a href="<?= url('/admin/items/create') ?>" class="btn btn-block"><i data-feather="plus-circle"></i> New Item</a></p>
        <p><a href="<?= url('/admin/items/fetch') ?>" class="btn btn-sm btn-gray">Fetch</a> <a href="<?= url('/admin/items/clean') ?>" class="btn btn-sm btn-gray">Clean</a></p>
    </nav>
</aside>
