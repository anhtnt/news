<?php view('admin.partials.header') ?>

<h1 class="page-title"><?= $title ?></h1>

<?php messages() ?>

<form method="POST" action="<?= url('/admin/settings') ?>" class="form-settings">
    <?php csrf() ?>

    <div class="row">
        <label for="site-title" class="col-md-3">Site title</label>
        <div class="col-md-9">
            <input type="text" name="site_title" id="site-title" value="<?= $settings['site_title'] ?>">
        </div>
    </div>

    <div class="row">
        <label for="site-description" class="col-md-3">Site description</label>
        <div class="col-md-9">
            <input type="text" name="site_description" id="site-description" value="<?= $settings['site_description'] ?>">
        </div>
    </div>

    <div class="row">
        <label for="feedly-user-id" class="col-md-3">Feedly user ID</label>
        <div class="col-md-9">
            <input type="text" name="feedly_user_id" id="feedly-user-id" value="<?= $settings['feedly_user_id'] ?>">
        </div>
    </div>

    <div class="row">
        <label for="feedly-access-token" class="col-md-3">Feedly access token</label>
        <div class="col-md-9">
            <textarea name="feedly_access_token" id="feedly-access-token"><?= $settings['feedly_access_token'] ?></textarea>
            <small><a href="https://feedly.com/v3/auth/dev" target="_blank">Click here</a> to get.</small>
        </div>
    </div>

    <div class="row">
        <label for="feedly-limit" class="col-md-3">Number of items</label>
        <div class="col-md-9">
            <input type="number" name="feedly_limit" id="feedly-limit" value="<?= $settings['feedly_limit'] ?>">
        </div>
    </div>

    <div class="row">
        <label for="disable-registration" class="col-md-3">Disable registration?</label>
        <div class="col-md-9">
            <input type="checkbox" name="disable_registration" id="disable-registration" value="1"<?= $settings['disable_registration'] ? ' checked' : '' ?>>
        </div>
    </div>

    <button>Update</button>
</form>

<?php view('admin.partials.footer') ?>
