<?php view('partials.header', ['title' => 'Submit - ' . setting('site_title', config('site.title'))]) ?>

<form method="post" action="<?= url('/submit') ?>">
    <h2>Submit a new article</h2>
    <?php messages() ?>
    <?php csrf() ?>

    <p>
        <label for="url">Article URL</label>
        <input id="url" type="url" name="url" required autofocus>
    </p>
    <p>
        <label for="title">Article title</label>
        <input id="title" type="text" name="title">
    </p>
    <p>
        <button>Submit</button>
    </p>
</form>

<?php view('partials.footer') ?>
