<?php $title = setting('site_title', config('site.title')) . ' - ' . setting('site_description') ?>
<?php view('partials.header', ['title' => $title]) ?>

<section>
    <?php foreach ($items as $item): ?>
        <article>
            <span><?= $item['views'] ?></span>
            <h2>
                <a href="<?= url("/item/{$item['id']}") ?>" target="_blank"><?= $item['title'] ?></a>
                <small><?= $item['domain'] ?> &ndash; <?= $item['formattedTime'] ?></small>
            </h2>
        </article>
    <?php endforeach ?>
</section>

<?php view('partials.pagination') ?>

<?php view('partials.footer') ?>
