</main>
<footer>
    &copy; <?= date('Y') ?> <?= setting('site_title', config('site.title')); ?>. Made with &hearts; by <a href="https://elightup.com">eLightUp</a>,
        the creator of <a href="https://metabox.io">Meta Box</a> and <a href="https://gretathemes.com">GretaThemes</a>.
</footer>
</body>
</html>
