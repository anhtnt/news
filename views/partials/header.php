<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title><?= $title ?></title>

        <link href="<?= url('/style.css') ?>" rel="stylesheet">
    </head>
    <body>
        <header>
            <h1><a href="<?= url() ?>"><?= setting('site_title', config('site.title')); ?></a></h1>
            <form method="get" action="<?= url('/') ?>">
                <input name="s" placeholder="Search..." value="<?= filter_input(INPUT_GET, 's') ?>">
            </form>
            <nav>
                <a href="<?= url('/submit') ?>">Submit</a>
            </nav>
        </header>
        <main>
