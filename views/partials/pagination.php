<?php
if (2 > $pagination['total']) {
    return;
}
?>
<nav>
    <ul class="pagination">
        <?php $uri = app('Atom\Http\Uri'); ?>

        <?php if ($pagination['start'] >= 2): ?>
            <li><a href="<?= $uri->removeQueryArg('page') ?>">1</a></li>
        <?php endif ?>
        <?php if ($pagination['start'] > 2): ?>
            <li>...</li>
        <?php endif ?>

        <?php for ($i = $pagination['start']; $i < $pagination['current']; $i++ ): ?>
            <li><a href="<?= 1 === $i ? $uri->removeQueryArg('page') : $uri->addQueryArg('page', $i) ?>"><?= $i ?></a></li>
        <?php endfor ?>
        <li><?= $pagination['current'] ?></li>
        <?php for ($i = $pagination['current'] + 1; $i <= $pagination['end']; $i++ ): ?>
            <li><a href="<?= $uri->addQueryArg('page', $i) ?>"><?= $i ?></a></li>
        <?php endfor ?>

        <?php if ($pagination['end'] < $pagination['total'] - 2): ?>
            <li><span>...</span></li>
        <?php endif ?>
        <?php if ($pagination['end'] <= $pagination['total'] - 2): ?>
            <li><a href="<?= $uri->addQueryArg('page', $pagination['total']) ?>"><?= $pagination['total'] ?></a></li>
        <?php endif ?>
    </ul>
</nav>
