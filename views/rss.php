<?= '<?' ?>xml version="1.0"?>
<rss version="2.0">
    <channel>
        <title><?= setting('site_title', config('site.title')); ?></title>
        <link><?= url() ?></link>
        <language>en</language>
        <lastBuildDate><?= date(DateTime::RSS) ?></lastBuildDate>

        <?php foreach ($items as $item): ?>
            <item>
                <title><?= $item['title'] ?></title>
                <link><?= url("/item/{$item['id']}") ?></link>
                <pubDate><?= date(DateTime::RSS, strtotime($item['date'])) ?></pubDate>
            </item>
        <?php endforeach ?>
    </channel>
</rss>
